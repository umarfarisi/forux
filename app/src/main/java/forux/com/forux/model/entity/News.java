package forux.com.forux.model.entity;

import java.util.Date;

/**
 * Created by Farisi on 27/08/2016.
 */
public class News {
    private String title;
    private String photo;
    private String video;
    private String outline;
    private String source;
    private String link;
    private long postTime;

    public News(String title, String photo, String video, String outline, String source, String link, long postTime) {
        this.title = title;
        this.photo = photo;
        this.outline = outline;
        this.source = source;
        this.link = link;
        this.postTime = postTime;
        this.video = video;
    }

    public News(String title) {
        this(title,null,null,null,null,null,(new Date()).getTime());
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getOutline() {
        return outline;
    }

    public void setOutline(String outline) {
        this.outline = outline;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public long getPostTime() {
        return postTime;
    }

    public void setPostTime(long postTime) {
        this.postTime = postTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        News news = (News) o;

        if (postTime != news.postTime) return false;
        if (!title.equals(news.title)) return false;
        if (photo != null ? !photo.equals(news.photo) : news.photo != null) return false;
        if (video != null ? !video.equals(news.video) : news.video != null) return false;
        if (!outline.equals(news.outline)) return false;
        if (!source.equals(news.source)) return false;
        return link.equals(news.link);

    }

    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + (photo != null ? photo.hashCode() : 0);
        result = 31 * result + (video != null ? video.hashCode() : 0);
        result = 31 * result + outline.hashCode();
        result = 31 * result + source.hashCode();
        result = 31 * result + link.hashCode();
        result = 31 * result + (int) (postTime ^ (postTime >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "News{" +
                "title='" + title + '\'' +
                ", photo='" + photo + '\'' +
                ", video='" + video + '\'' +
                ", outline='" + outline + '\'' +
                ", source='" + source + '\'' +
                ", link='" + link + '\'' +
                ", postTime=" + postTime +
                '}';
    }
}
