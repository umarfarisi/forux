package forux.com.forux.model.intractor;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import forux.com.forux.model.entity.News;
import forux.com.forux.presenter.NewsPresenter;
import forux.com.forux.utils.Config;

/**
 * Created by Asus on 22/01/2017.
 */
public class NewsIntractor {

    public static final int MODE_RELOAD = 0;
    public static final int MODE_FIRST = 1;
    private static final String COUNT = "count";
    private static final String DATA = "data";
    private static final String TITLE = "title";
    private static final String OUTLINE = "outline";
    private static final String POST_TIME = "post_time";
    private static final String PHOTO = "photo";
    private static final String SOURCE = "source";
    private static final String LINK = "link";
    private static final String VIDEO = "video";

    private int count;
    private NewsPresenter presenter;

    public NewsIntractor(NewsPresenter presenter) {
        this.presenter = presenter;
    }

    public void getNews(int start, int count, int mode, String category) {
        //menyetel url yang akan digunakan di get news helper
        String url = Config.getDataURL(category,start,count);
        getNewsHelper(url, mode);
    }

    private void getNewsHelper(String url, final int mode){
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                List<News> newses = parserJSON(response);
                switch (mode){
                    case NewsIntractor.MODE_FIRST:
                        presenter.onLoadNewsSuccess(newses);
                        break;
                    case NewsIntractor.MODE_RELOAD:
                        presenter.onReloadNewsSuccess(newses);
                        break;
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                switch (mode){
                    case NewsIntractor.MODE_FIRST:
                        presenter.onLoadNewsFailed();
                        break;
                    case NewsIntractor.MODE_RELOAD:
                        presenter.onReloadNewsFailed();
                        break;
                }
            }
        });
        Volley.newRequestQueue(presenter.getContext()).add(request);
    }

    public void getNewsUpdate(String category) {
        String url = Config.getUpdateURL(category,count);
        getNewsUpdateHelper(url);
    }

    private void getNewsUpdateHelper(String url){
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                List<News> newses = parserJSON(response);
                presenter.onUpdateNewsSuccess(newses);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                presenter.onUpdateNewsFailed();
            }
        });
        Volley.newRequestQueue(presenter.getContext()).add(request);
    }

    private List<News> parserJSON(String response) {

        List<News> newses = new ArrayList<>();
        try {
            JSONObject root = new JSONObject(response);
            count = root.getInt(COUNT);
            JSONArray data = root.getJSONArray(DATA);
            for(int i = 0 ; i < data.length() ; i++){
                JSONObject newsJO = data.getJSONObject(i);
                String title = newsJO.getString(TITLE);
                String outline = newsJO.getString(OUTLINE);
                long postTime = newsJO.getLong(POST_TIME);
                String video = null;
                String photo = null;
                if(!newsJO.getString(VIDEO).equals(""))
                    video = newsJO.getString(VIDEO);
                else
                    photo = newsJO.getString(PHOTO);
                String source = newsJO.getString(SOURCE);
                String link = newsJO.getString(LINK);
                News news = new News(title,photo,video,outline,source,link,postTime);
                newses.add(news);
            }
        } catch (JSONException e) {
            Log.e(Config.ERROR_LOG,"NewsIntractor - parserJSON - message:"+e.getMessage());
        }

        return newses;
    }

}
