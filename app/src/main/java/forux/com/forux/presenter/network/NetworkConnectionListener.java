package forux.com.forux.presenter.network;

/**
 * Created by Asus on 24/01/2017.
 */
public class NetworkConnectionListener {
    private boolean isConnected;

    public NetworkConnectionListener(boolean isConnected) {
        this.isConnected = isConnected;
    }

    public boolean isConnected() {
        return isConnected;
    }
}
