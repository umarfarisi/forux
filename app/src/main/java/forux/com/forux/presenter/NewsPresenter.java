package forux.com.forux.presenter;

import android.content.Context;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import forux.com.forux.model.entity.News;
import forux.com.forux.model.intractor.NewsIntractor;
import forux.com.forux.presenter.network.NetworkConnectionListener;
import forux.com.forux.view.interfaces.NewsView;

/**
 * Created by Asus on 22/01/2017.
 */
public class NewsPresenter {
    private final int BASE_START = 0;
    private final int COUNT = 10;
    private Context context;
    private NewsView view;
    private NewsIntractor model;
    private int start;

    public NewsPresenter(Context context){
        this.context = context;
    }

    //Akan diimplementasikan oleh subclass dan akan digunakan oleh view
    public void attachView(NewsView view){
        this.view = view;
        this.model = new NewsIntractor(this);
        EventBus.getDefault().register(this);
    }

    //Akan digunakan oleh kelas model
    public Context getContext() {
        return context;
    }

    //Akan digunakan oleh subclass
    protected boolean isViewAttached(){
        return view != null;
    }

    //Akan dipanggil oleh class view
    public void updateNews() {
        if(isViewAttached()){
            model.getNewsUpdate(view.getCategory());
        }
    }
    public void loadNews() {
        if(isViewAttached()){
            view.showProgressSign();
            model.getNews(start,COUNT,NewsIntractor.MODE_FIRST, view.getCategory());
        }
    }
    public void reloadNews(){
        if(isViewAttached()){
            view.showReloadSign();
            start += COUNT;
            model.getNews(start,COUNT,NewsIntractor.MODE_RELOAD, view.getCategory());
        }
    }
    public void detachView() {
        view = null;
        model = null;
        EventBus.getDefault().unregister(this);
    }

    //Akan dipanggil oleh class model sebagai result
    public void onLoadNewsSuccess(List<News> newses) {
        if(isViewAttached()){
            view.hideProgressSign();
            if(!newses.isEmpty()) {
                view.hideEmptySign();
                view.displayNews(newses);
            }else {
                view.showEmptySign();
            }
        }else{
            Log.e("NEWSPRESENTER","BLUM ATTACH VIEW");
        }
    }
    public void onLoadNewsFailed() {
        if(isViewAttached()){
            view.hideProgressSign();
            view.showEmptySign();
        }else{
            Log.e("NEWSPRESENTER","BLUM ATTACH VIEW");
        }
    }
    public void onUpdateNewsSuccess(List<News> newses){
        if(isViewAttached()){
            if(!newses.isEmpty()){
                view.hideEmptySign();
                view.alertUpdate(newses);
            }
        }
    }
    public void onUpdateNewsFailed(){
    }
    public void onReloadNewsSuccess(List<News> newses){
        if(isViewAttached()){
            view.hideReloadSign();
            if(!newses.isEmpty()){
                view.reloadNews(newses);
            }else{
                view.reloadNewsEmpty();
            }
        }
    }
    public void onReloadNewsFailed(){
        if(isViewAttached()){
            view.hideReloadSign();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNetworkConnectionChange(NetworkConnectionListener listener){
        boolean isConnected = listener.isConnected();
        if(isConnected && start == BASE_START){
            loadNews();
        }
        Log.d("Networkkk","SUBCRIBE "+isConnected+", start: "+start);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdateNews(Boolean[] times){
        updateNews();
    }

}
