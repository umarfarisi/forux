package forux.com.forux.presenter.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Asus on 24/01/2017.
 */
public class NetworkConnectionRecaiver extends BroadcastReceiver {

    public static final String CHECK_FIRST_TIME_CONNECTION = "check first time connection";

    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent != null){
            if(intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)
                    || intent.getAction().equals(CHECK_FIRST_TIME_CONNECTION)){
                ConnectivityManager cm =(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected = activeNetwork != null &&
                        activeNetwork.isConnectedOrConnecting();

                EventBus.getDefault().post(new NetworkConnectionListener(isConnected));
                Log.d("Networkkk","RECAIVER");
            }
        }
    }
}
