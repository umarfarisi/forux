package forux.com.forux.presenter;

import android.os.AsyncTask;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import forux.com.forux.presenter.network.NetworkConnectionListener;
import forux.com.forux.view.interfaces.MainView;

/**
 * Created by Asus on 29/01/2017.
 */
public class MainPresenter {

    private boolean isLoading;
    private boolean isConnected;
    private TimerTask timerTask;
    private MainView view;

    public void attachView(MainView view){
        this.view = view;
        EventBus.getDefault().register(this);
    }

    public void detachView(){
        this.view = null;
        EventBus.getDefault().unregister(this);
    }

    public void startTimer(){
        isLoading = true;
        executeTimerTask();
    }

    private void executeTimerTask() {
        timerTask = new TimerTask();
        timerTask.execute();
    }

    public void stopTimer(){
        isLoading = false;
        timerTask = null;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onNetworkConnectionChange(NetworkConnectionListener listener){
        isConnected = listener.isConnected();
        if(isConnected){
            view.hideNetworkDisconnection();
            executeTimerTask();
        }else{
            view.showNetworkDisconnection();
        }
        Log.d("Networkkk","SUBCRIBE");
    }

    private class TimerTask extends AsyncTask<Void,Boolean,Void> {

        private static final long TIMER_SLEEP = 600000; //10 menit loop

        @Override
        protected Void doInBackground(Void... times) {
            while(isLoading && isConnected){
                try {
                    Thread.sleep(TIMER_SLEEP);
                    publishProgress(new Boolean[]{});
                } catch (InterruptedException e) {
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Boolean... times) {
            EventBus.getDefault().post(times);
        }


    }
}
