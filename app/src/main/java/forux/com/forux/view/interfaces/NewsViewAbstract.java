package forux.com.forux.view.interfaces;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import forux.com.forux.R;
import forux.com.forux.model.entity.News;
import forux.com.forux.presenter.NewsPresenter;

/**
 * Created by Asus on 30/01/2017.
 */
public abstract class NewsViewAbstract extends Fragment implements NewsView{

    @BindView(R.id.newsRV)
    protected RecyclerView newsRV;
    @BindView(R.id.emptyTV)
    protected TextView emptyTV;
    @BindView(R.id.progressPB)
    protected ProgressBar progressPB;
    @BindView(R.id.updateBtn)
    protected Button updateBtn;
    protected Button reloadBtn;
    protected ProgressBar reloadPB;

    protected Unbinder unbinder;

    protected NewsPresenter presenter;

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView();
        unbinder.unbind();
    }

    @Override
    public void showEmptySign() {
        emptyTV.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptySign() {
        emptyTV.setVisibility(View.GONE);
    }

    @Override
    public void showProgressSign() {
        progressPB.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressSign() {
        progressPB.setVisibility(View.GONE);
    }

    @Override
    public void hideReloadSign() {
        if(reloadPB == null || reloadBtn == null)return;
        reloadPB.setVisibility(View.GONE);
        reloadBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void showReloadSign() {
        if(reloadPB == null || reloadBtn == null)return;
        reloadBtn.setVisibility(View.GONE);
        reloadPB.setVisibility(View.VISIBLE);
    }

    @Override
    public void displayNews(List<News> newses) {
        NewsAdapter adapter = (NewsAdapter) newsRV.getAdapter();
        adapter.setList(newses);
    }

    @Override
    public void reloadNews(List<News> newses) {
        NewsAdapter adapter = (NewsAdapter) newsRV.getAdapter();
        adapter.addList(newses);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void reloadNewsEmpty() {
        Toast.makeText(getContext(),"No more old news",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void alertUpdate(final List<News> newses) {
        //memunculkan tombol untuk melihat semua update
        updateBtn.setVisibility(View.VISIBLE);

        //aksi ketika tombol update diklik
        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setVisibility(View.GONE);
                //TODO focus kembali ke entry pertama (entry paling atas)
                //menambahkan data ke list
                NewsAdapter adapter = (NewsAdapter) newsRV.getAdapter();
                adapter.addList(newses, 0);
                //notifikasi jika ada perubahan pada data
                adapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter.loadNews();
    }

    public abstract class NewsAdapter<T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T>{

        protected final int EXTRA_INDEX = 1;
        protected List<News> newses;
        protected List<News> newsesUpdate;

        public NewsAdapter() {
            this.newses = new ArrayList<>();
        }

        public void setList(List<News> newses){
            this.newses = newses;
            notifyDataSetChanged();
        }


        public void addList(List<News> newses){
            this.newses.addAll(newses);
        }


        public void addList(List<News> newses, int index){
            this.newses.addAll(index,newses);
        }

        @Override
        public int getItemCount() {
            if(!newses.isEmpty())return newses.size()+EXTRA_INDEX;
            else return newses.size();
        }

        public class ReloadNewsVH extends RecyclerView.ViewHolder{

            @BindView(R.id.reloadBtn) public Button reloadBtn;
            @BindView(R.id.reloadPB) public ProgressBar reloadPB;

            public ReloadNewsVH(View itemView) {
                super(itemView);
                ButterKnife.bind(this,itemView);
            }
        }

    }

}
