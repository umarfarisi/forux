package forux.com.forux.view.interfaces;

import java.util.List;

import forux.com.forux.model.entity.News;

/**
 * Created by Asus on 22/01/2017.
 */
public interface NewsView {
    void displayNews(List<News> newses);
    void showEmptySign();
    void hideEmptySign();
    void showProgressSign();
    void hideProgressSign();

    void hideReloadSign();
    void showReloadSign();
    void reloadNews(List<News> newses);
    void reloadNewsEmpty();

    void alertUpdate(List<News> newses);

    String getCategory();

}
