package forux.com.forux.view.impl;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import forux.com.forux.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LiveCoverageFragment extends Fragment {


    public LiveCoverageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_live_coverage, container, false);
    }

}
