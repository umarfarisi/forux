package forux.com.forux.view.impl;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import forux.com.forux.R;
import forux.com.forux.model.entity.News;
import forux.com.forux.presenter.NewsPresenter;
import forux.com.forux.utils.Config;
import forux.com.forux.view.interfaces.NewsViewAbstract;

/**
 * A simple {@link Fragment} subclass.
 */
public class BriefingFragment extends NewsViewAbstract{


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = new NewsPresenter(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_briefing, container, false);

        unbinder = ButterKnife.bind(this, view);

        //menyetel layout manager dan adapter
        newsRV.setLayoutManager(new LinearLayoutManager(getContext()));
        newsRV.setAdapter(new BriefingNewsAdapter());

        presenter.attachView(this);
        return view;
    }

    @Override
    public String getCategory() {
        return Config.CATEGORY_BRIEFING;
    }


    class BriefingNewsAdapter extends NewsAdapter<RecyclerView.ViewHolder>{

        private final int RELOAD_VIEW = 0;
        private final int WITH_PHOTO = 1;
        private final int WITH_VIDEO = 2;

        @Override
        public int getItemViewType(int position) {
            boolean isLastPosition = position==getItemCount()-EXTRA_INDEX;
            if(isLastPosition){
                return RELOAD_VIEW;
            }else{
                boolean isWithVideo = newses.get(position).getVideo() != null;
                if(isWithVideo){
                    return WITH_VIDEO;
                }else{
                    return WITH_PHOTO;
                }
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view;
            switch (viewType){
                case RELOAD_VIEW:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_reload_oldnews, parent, false);
                    return new ReloadNewsVH(view);
                case WITH_PHOTO:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_briefing_with_photo, parent, false);
                    return new BriefingNewsVHWithPhoto(view);
                default:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_briefing_with_video, parent, false);
                    return new BriefingNewsVHWithVideo(view);
            }
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

            if (holder instanceof BriefingNewsVHWithPhoto){
                BriefingNewsVHWithPhoto briefingNewsVH = ((BriefingNewsVHWithPhoto)holder);
                final News news = newses.get(position);

                Picasso.with(BriefingFragment.this.getContext()).load(news.getPhoto()).
                        placeholder(R.drawable.photo_not_available).into(briefingNewsVH.photoIV);
                briefingNewsVH.titleTV.setText(news.getTitle());
                briefingNewsVH.outlineTV.setText(news.getOutline());
                briefingNewsVH.linkTV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(news.getLink()));
                        BriefingFragment.this.getActivity().startActivity(browserIntent);
                    }
                });
            }else if(holder instanceof BriefingNewsVHWithVideo){

                final BriefingNewsVHWithVideo briefingNewsVH = ((BriefingNewsVHWithVideo)holder);
                final News news = newses.get(position);

                Log.d("VIDEOOO","NEWS:"+news);

                briefingNewsVH.titleTV.setText(news.getTitle());
                briefingNewsVH.outlineTV.setText(news.getOutline());
                briefingNewsVH.linkTV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(news.getLink()));
                        BriefingFragment.this.getActivity().startActivity(browserIntent);
                    }
                });

                briefingNewsVH.playButtonIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragmentActivity activity = BriefingFragment.this.getActivity();
                        Intent videoIntent = YouTubeStandalonePlayer.createVideoIntent(activity, Config.YOUTUBE_API_KEY, news.getVideo());
                        activity.startActivity(videoIntent);
                    }
                });
                briefingNewsVH.videoYTV.initialize(Config.YOUTUBE_API_KEY, new YouTubeThumbnailView.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader youTubeThumbnailLoader) {
                        youTubeThumbnailLoader.setVideo(news.getVideo().substring(news.getVideo().lastIndexOf("watch?v=")+8));
                        youTubeThumbnailLoader.setOnThumbnailLoadedListener(new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {
                            @Override
                            public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String s) {
                                briefingNewsVH.playButtonIV.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {
                                briefingNewsVH.videoYTV.setImageResource(R.drawable.video_not_available);
                            }
                        });
                    }

                    @Override
                    public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {
                        String error = youTubeInitializationResult.toString();
                        Toast.makeText(BriefingFragment.this.getContext(), error, Toast.LENGTH_LONG).show();
                        briefingNewsVH.videoYTV.setImageResource(R.drawable.video_not_available);
                    }
                });
                briefingNewsVH.videoYTV.setVisibility(View.VISIBLE);

            }else{
                ReloadNewsVH reloadNewsVH = (ReloadNewsVH) holder;
                reloadBtn = reloadNewsVH.reloadBtn;
                reloadPB = reloadNewsVH.reloadPB;

                reloadNewsVH.reloadBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        presenter.reloadNews();
                    }
                });


            }

        }

        class BriefingNewsVHWithPhoto extends RecyclerView.ViewHolder{

            @BindView(R.id.photoIV) ImageView photoIV;
            @BindView(R.id.titleTV) TextView titleTV;
            @BindView(R.id.outlineTV) TextView outlineTV;
            @BindView(R.id.linkTV) TextView linkTV;

            public BriefingNewsVHWithPhoto(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }

        class BriefingNewsVHWithVideo extends RecyclerView.ViewHolder{

            @BindView(R.id.playButtonIV) ImageView playButtonIV;
            @BindView(R.id.videoYTV) YouTubeThumbnailView videoYTV;
            @BindView(R.id.titleTV) TextView titleTV;
            @BindView(R.id.outlineTV) TextView outlineTV;
            @BindView(R.id.linkTV) TextView linkTV;

            public BriefingNewsVHWithVideo(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }

    }

}
