package forux.com.forux.view.interfaces;

/**
 * Created by Asus on 30/01/2017.
 */
public interface MainView {
    void showNetworkDisconnection();
    void hideNetworkDisconnection();
}
