package forux.com.forux.view.impl;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import forux.com.forux.R;
import forux.com.forux.presenter.MainPresenter;
import forux.com.forux.presenter.network.NetworkConnectionRecaiver;
import forux.com.forux.view.interfaces.MainView;

public class MainActivity extends AppCompatActivity implements MainView {

    @BindView(R.id.root) CoordinatorLayout root;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.tabLayout) TabLayout tabLayout;
    @BindView(R.id.viewPager) ViewPager viewPager;

    private Snackbar connectionAlert;
    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new MainPresenter();

        ButterKnife.bind(this);

        //menyetel connection alert
        connectionAlert = Snackbar.make(root,"Network is disconnect",Snackbar.LENGTH_INDEFINITE);

        //menyetel adapter untuk viewPager
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new BriefingFragment(), "Briefing");
        adapter.addFragment(new EditorialFragment(), "Editorial");
//        adapter.addFragment(new LiveCoverageFragment(),"Live Coverage");
        viewPager.setAdapter(adapter);

        //membuat view pager tidak men destroy fragmentnya, 2 fragment dari fragment yg terlihat tidak akan di destry
        viewPager.setOffscreenPageLimit(2);

        tabLayout.setupWithViewPager(viewPager);

        setSupportActionBar(toolbar);

        //mengecek network
        Intent intent = new Intent(this, NetworkConnectionRecaiver.class);
        intent.setAction(NetworkConnectionRecaiver.CHECK_FIRST_TIME_CONNECTION);
        sendBroadcast(intent);

    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.attachView(this);
        presenter.startTimer();
    }

    @Override
    protected void onStop() {
        presenter.stopTimer();
        presenter.detachView();
        super.onStop();
    }

    @Override
    public void showNetworkDisconnection() {
        if(!connectionAlert.isShown())connectionAlert.show();
    }

    @Override
    public void hideNetworkDisconnection() {
        if(connectionAlert.isShown())connectionAlert.dismiss();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter{

        private List<Fragment> fragments;
        private List<String> titles;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            fragments = new ArrayList<>();
            titles = new ArrayList<>();
        }

        public void addFragment(Fragment fragment, String title){
            fragments.add(fragment);
            titles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }


}
