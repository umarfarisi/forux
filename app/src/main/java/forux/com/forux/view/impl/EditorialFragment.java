package forux.com.forux.view.impl;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import forux.com.forux.R;
import forux.com.forux.model.entity.News;
import forux.com.forux.presenter.NewsPresenter;
import forux.com.forux.utils.Config;
import forux.com.forux.view.interfaces.NewsViewAbstract;


public class EditorialFragment extends NewsViewAbstract{

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        presenter = new NewsPresenter(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_editorial, container, false);

        unbinder = ButterKnife.bind(this, view);

        //menyetel layout manager dan adapter
        newsRV.setLayoutManager(new LinearLayoutManager(getContext()));
        newsRV.setAdapter(new EditorialNewsAdapter());

        presenter.attachView(this);

        return view;
    }

    @Override
    public String getCategory() {
        return Config.CATEGORY_EDITORIAL;
    }

    class EditorialNewsAdapter extends NewsAdapter<RecyclerView.ViewHolder>{

        private final int RELOAD_VIEW = 0;
        private final int WITH_PHOTO = 1;
        private final int WITH_VIDEO = 2;

        @Override
        public int getItemViewType(int position) {
            boolean isLastPosition = position==getItemCount()-EXTRA_INDEX;
            if(isLastPosition){
                return RELOAD_VIEW;
            }else {
                News news = newses.get(position);
                boolean isWithPhoto = news.getVideo() == null;
                return isWithPhoto ? WITH_PHOTO : WITH_VIDEO;
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder holder = null;
            View view = null;

            switch (viewType){
                case WITH_PHOTO:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_editorial_with_photo, parent, false);
                    holder = new EditorialNewsVHWithPhoto(view);
                    break;
                case WITH_VIDEO:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_editorial_with_video, parent, false);
                    holder = new EditorialNewsVHWithVideo(view);
                    break;
                case RELOAD_VIEW:
                    view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_reload_oldnews, parent, false);
                    holder = new ReloadNewsVH(view);
            }

            return holder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder vh, int position) {

            if(vh instanceof EditorialNewsVHWithPhoto) {

                final News news = newses.get(position);

                EditorialNewsVHWithPhoto holder = (EditorialNewsVHWithPhoto) vh;

                Picasso.with(EditorialFragment.this.getContext()).load(news.getPhoto()).
                        placeholder(R.drawable.photo_not_available).into(holder.photoIV);
                holder.titleTV.setText(news.getTitle());
                holder.linkTV.setText(news.getLink());

                holder.linkTV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(news.getLink()));
                        EditorialFragment.this.getActivity().startActivity(browserIntent);
                    }
                });

            }else if(vh instanceof EditorialNewsVHWithVideo){

                final News news = newses.get(position);
                final EditorialNewsVHWithVideo holder = (EditorialNewsVHWithVideo) vh;

                holder.playButtonIV.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragmentActivity activity = EditorialFragment.this.getActivity();
                        Intent videoIntent = YouTubeStandalonePlayer.createVideoIntent(activity, Config.YOUTUBE_API_KEY, news.getVideo());
                        activity.startActivity(videoIntent);
                    }
                });

                holder.videoYTV.initialize(Config.YOUTUBE_API_KEY, new YouTubeThumbnailView.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader youTubeThumbnailLoader) {
                        youTubeThumbnailLoader.setVideo(news.getVideo().substring(news.getVideo().lastIndexOf("watch?v=")+8));
                        youTubeThumbnailLoader.setOnThumbnailLoadedListener(new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {
                            @Override
                            public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String s) {
                                holder.playButtonIV.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {
                                holder.videoYTV.setImageResource(R.drawable.video_not_available);
                            }
                        });
                    }

                    @Override
                    public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {
                        String error = youTubeInitializationResult.toString();
                        Toast.makeText(EditorialFragment.this.getContext(), error, Toast.LENGTH_LONG).show();
                        holder.videoYTV.setImageResource(R.drawable.video_not_available);
                    }
                });

                holder.videoYTV.setVisibility(View.VISIBLE);
                holder.titleTV.setText(news.getTitle());
                holder.outlineTV.setText(news.getOutline());
            }else{

                ReloadNewsVH holder = (ReloadNewsVH) vh;

                reloadBtn = holder.reloadBtn;
                reloadPB = holder.reloadPB;

                holder.reloadBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        presenter.reloadNews();
                    }
                });


            }
        }

        class EditorialNewsVHWithPhoto extends RecyclerView.ViewHolder{

            @BindView(R.id.photoIV) ImageView photoIV;
            @BindView(R.id.titleTV) TextView titleTV;
            @BindView(R.id.linkTV) TextView linkTV;

            public EditorialNewsVHWithPhoto(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }

        class EditorialNewsVHWithVideo extends RecyclerView.ViewHolder{

            @BindView(R.id.titleTV) TextView titleTV;
            @BindView(R.id.outlineTV) TextView outlineTV;
            @BindView(R.id.playButtonIV) ImageView playButtonIV;
            @BindView(R.id.videoYTV) YouTubeThumbnailView videoYTV;

            public EditorialNewsVHWithVideo(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }

    }
}
